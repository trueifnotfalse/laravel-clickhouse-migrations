<?php

namespace Sagirba\ClickhouseMigrations\Migrations;

class FileModel
{
    /**
     * @var string
     */
    protected string $workDir = '';

    /**
     * File model constructor
     */
    public function __construct()
    {
        $this->workDir = getcwd();
    }

    /**
     * @param string $path
     *
     * @return string
     */
    public function read(string $path): string
    {
        return (string)file_get_contents($this->workDir . DIRECTORY_SEPARATOR . $path);
    }

    /**
     * @param string $path
     * @param string $contents
     *
     * @return bool
     */
    public function put(string $path, string $contents): bool
    {
        return (bool)file_put_contents($this->workDir . DIRECTORY_SEPARATOR . $path, $contents);
    }

    /**
     * @param string $directory
     * @param bool   $hidden
     *
     * @return array
     */
    public function files(string $directory, bool $hidden = false): array
    {
        $fullPath = $this->normalizePath($this->getPathPrefix() . $directory);

        return iterator_to_array(
            \Symfony\Component\Finder\Finder::create()->files()->ignoreDotFiles(! $hidden)->in($fullPath)->depth(0)->sortByName(),
            false
        );
    }

    /**
     * @param string $path
     */
    public function getFullPath(string $path): string
    {
        return $this->normalizePath($this->getPathPrefix() . $path);
    }

    /**
     * @return string
     */
    public function getPathPrefix(): string
    {
        return $this->workDir;
    }

    /**
     * @param string $path
     *
     * @return string
     */
    protected function normalizePath(string $path): string
    {
        return str_replace('//', '/', $path);
    }
}
